<?php

namespace App\Http\Controllers\Api;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login()
    {
        $request = request();
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            if (! $token = JWTAuth::attempt($request->only(['email', 'password']))) {
                $this->apiResponse(401,'invalid credentials');
            }
        } catch (JWTException $e) {
            $this->apiResponse(500, 'Could not create token');
        }

        return $this->apiResponse(200, $token);
    }
}
