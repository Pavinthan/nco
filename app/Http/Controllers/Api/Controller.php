<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Serializer\DataArraySerializer;

class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function transform($data, $transformer, array $includes = [])
    {
        if (class_basename($data) == 'Collection') {
            $resource = new Collection($data, $transformer);
        } else {
            $resource = new Item($data, $transformer);
        }
        $manager = new Manager();
        $manager->parseIncludes($includes);
        $manager->setSerializer(new DataArraySerializer());
        return $manager->createData($resource)->toArray();
    }

    public function apiResponse($code, $message)
    {
        return response([
            'message' => $message,
            'status_code' => $code
        ], $code);
    }
}

