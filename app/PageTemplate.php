<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTemplate extends Model
{
    protected  $fillable = ['name','status','description'];

    public function pages()
    {
        return $this->hasMany('App\Page');
    }
}
