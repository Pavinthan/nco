<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommender extends Model
{
    use SoftDeletes;

    protected $table = 'recommendables';

    protected $fillable = ['recommendable_type', 'recommendable_id', 'user_id'];

    public function user()
    {
        return $this->morphTo(User::class, 'recommendable');
    }
}
