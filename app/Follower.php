<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Follower extends Model
{
    use SoftDeletes;

    protected $table = 'followables';

    protected $fillable = ['followable_type', 'followable_id', 'user_id',];

    public function user()
    {
        return $this->morphTo(User::class, 'followable');
    }
}
