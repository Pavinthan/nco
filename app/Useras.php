<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Useras extends Model
{
    protected $fillable = ['first_name', 'last_name', 'short_name', 'email', 'dob', 'profile_image', 'is_active', 'description'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsToMany('App\Category');
    }
}
