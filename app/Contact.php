<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['email','department','phone','home_phone','other_phone','fax','mobile','dob','assistant','assistant_phone','reports_to','skype','secondary_email','twitter','description'];

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }
}
