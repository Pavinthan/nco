<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['name', 'description'];

    public function staff()
    {
        return $this->belongsToMany(Staff::class)->withPivot(['parent_id']);
    }

    public function organizations()
    {
        return $this->belongsToMany(Organization::class);
    }
}
