<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['event_type_id', 'what', 'all_day', 'start', 'end', 'repeat', 'repeat_every', 'repeat_end', 'where', 'visibility', 'name', 'email', 'timezone'];
    protected $dates = ['start', 'end', 'repeat_end'];

    public function eventType()
    {
        return $this->belongsTo(EventType::class);
    }

    public function users()
    {
        return $this->morphedByMany('App\User', 'eventable');
    }

    public function organizations()
    {
        return $this->morphedByMany('App\Organization', 'eventable');
    }
}
