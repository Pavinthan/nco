<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    protected  $fillable = ['name','status','description', 'page_template_id'];

    public function users()
    {
        return $this->morphedByMany(User::class, 'pageable');
    }

    public function organizations()
    {
        return $this->morphedByMany(Organization::class, 'pageable');
    }

    public function pageTemplate()
    {
        return $this->belongsTo(PageTemplate::class);
    }
}
