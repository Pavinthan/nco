<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description'];

    public function titles()
    {
        return $this->hasMany('App\Title');
    }

    public function userAs()
    {
        return $this->belongsToMany('App\Useras');
    }
}
