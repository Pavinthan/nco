<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Connection extends Model
{
    use SoftDeletes;

    protected $table = 'connectionables';

    protected $fillable = ['connectionable_type', 'connectionable_id', 'user_id'];

    public function user()
    {
        return $this->morphedByMany(User::class, 'connectionable');
    }
}
