<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['name', 'description'];

    public function organizationType()
    {
        return $this->belongsTo('App\OrganizationType');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function recommenders()
    {
        return $this->morphToMany('App\Recommender', 'recommendable');
    }

    public function followers()
    {
        return $this->morphToMany('App\Follower', 'followable');
    }

    public function pages()
    {
        return $this->morphedByMany('App\Page', 'pageable');
    }

    public function requests()
    {
        return $this->morphedByMany('App\Request', 'requestable');
    }

    public function events()
    {
        return $this->morphedByMany('App\Event', 'eventable');
    }

    public function groups()
    {
        return $this->morphToMany(Group::class, 'groupable');
    }

    public function staff()
    {
        return $this->belongsToMany(Staff::class)->withPivot(['parent_id']);
    }

    public function designations()
    {
        return $this->belongsToMany(Designation::class);
    }
}
