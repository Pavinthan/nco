<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    protected $fillable = ['name', 'description'];
    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
