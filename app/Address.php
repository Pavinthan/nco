<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected  $fillable = ['address_type','address_line_1','address_line_2','address_line_3', 'city','state','postal_code','country_id'];

    public function addressable()
    {
        return $this->morphTo();
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
