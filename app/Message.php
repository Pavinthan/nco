<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'message','message_type'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function messageable()
    {
        return $this->morphTo();
    }

    public function getMessageAttribute($value)
    {
        return decrypt($value);
    }

    public function setMessageAttribute($value)
    {
        $this->attributes['message'] = encrypt($value);
    }
}
