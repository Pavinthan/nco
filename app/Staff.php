<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = ['first_name', 'last_name', 'short_name', 'email', 'dob', 'staff_type', 'profile_image', 'is_active', 'designation_id', 'description', 'telephone', 'mobile', 'skype'];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class)->withPivot(['parent_id']);
    }
    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }
    public function parent()
    {
        return $this->hasOne(Staff::class, 'id', 'parent_id');
    }
}
