<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requestables', function (Blueprint $table) {
            $table->unsignedInteger('requestable_id')->unsigned()->index();
            $table->string('requestable_type');
            $table->unsignedInteger('request_id')->unsigned()->index();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requestables');
    }
}
