<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventables', function (Blueprint $table) {
            $table->unsignedInteger('eventable_id')->unsigned()->index();
            $table->string('eventable_type');
            $table->unsignedInteger('event_id')->unsigned()->index();
            $table->text('value')->nullable();
            $table->text('description')->nullable();
            $table->enum('is_owner', ['Yes', 'No'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventables');
    }
}
