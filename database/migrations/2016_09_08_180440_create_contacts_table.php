<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->default(null);
            $table->string('email')->default(null);
            $table->string('department')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('home_phone')->nullable()->default(null);
            $table->string('other_phone')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('mobile')->nullable()->default(null);
            $table->date('dob')->nullable()->default(null);
            $table->string('assistant')->nullable()->default(null);
            $table->string('assistant_phone')->nullable()->default(null);
            $table->string('reports_to')->nullable()->default(null);
            $table->string('skype')->nullable()->default(null);
            $table->string('secondary_email')->nullable()->default(null);
            $table->string('twitter')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->unsignedInteger('user_id')->index()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
