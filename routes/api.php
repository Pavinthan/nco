<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('/login', ['as' => 'api.login', 'uses' => 'Api\AuthController@login']);
    Route::post('/register', ['as' => 'api.login', 'uses' => 'AuthController@register']);
//    Route::post('/logout', ['as' => 'api.logout', 'uses' => 'AuthController@logout']);

});
